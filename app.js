const express = require('express'),
  app = express(),
  fs = require('fs')

var port = process.env.PORT || 8080;

app.use(express.static('./public'))

app.get('/', (req, res) => {
  fs.readFile('./main.html', (err, data) => {
    if (err) res.end('Error during Loading file')
    res.end(data)
  })
})

app.use('*', (req, res) => {
  res.redirect('/')
})

app.listen(port)
