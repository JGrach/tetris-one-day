var VIEWER = {
  toDisplay: function() {
    const dom = document.getElementById('tetrisViewer')
    const size = {
      x: dom.offsetWidth / GAME_GRID_SIZE.x,
      y: dom.offsetHeight / GAME_GRID_SIZE.y,
    }
    var gridDom = ''
    GAME_GRID.lines.map(function(line) {
      gridDom +=
        '<div class=line style=height:' + Math.ceil(size.y) + 'px;></div>'
      var indexInsert = gridDom.length - 6
      line.map(function(square) {
        const className = square ? square.formId : 'empty'
        const squareDom =
          '<div class="square ' +
          className +
          '" style=width:' +
          Math.ceil(size.x) +
          'px;></div>'
        gridDom =
          gridDom.slice(0, indexInsert) + squareDom + gridDom.slice(indexInsert)
        indexInsert += squareDom.length
      })
    })
    dom.innerHTML = gridDom
  },
  toResult: function() {
    const domScore = document.getElementById('score')
    const domFormsNumber = document.getElementById('formsNumber')

    domScore.innerHTML = 'Score: ' + SCORE
    domFormsNumber.innerHTML =
      "Nombres d'éléments sur le terrain: " + FORM_NUMBER
  },
  toGameOver: function() {
    const domOver = document.getElementById('gameOver')
    domOver.hidden = false
  },
}
